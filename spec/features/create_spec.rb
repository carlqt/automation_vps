feature 'vps-staging', js: true do
  background do
    visit '/'
  end

  let(:timestamp) { Time.now.strftime('%Y-%m-%d_%H-%M-%S') }
  let(:dir) { ENV['dir'] }

  feature "Initial setup" do
    background do
      sign_in 'vps@dropmysite.com'
    end

    feature "create private server" do
      it 'successfully creates server and run the client' do
        find(:xpath, "//a[@href='/en/private_servers/new']").click

        create_server do |settings|
          settings[:directories] = add_dir(dir)
          settings[:exclusions] = add_exc(dir)
          settings[:distribution] = distro
          settings[:timestamp] = timestamp
          settings[:version] = version
        end

        file_url = find_link('here')[:href]
        install_and_run_vps(file_url)
        sleep 5

        visit '/en/dashboard'
        node = verify_in_all_pages("#{distro}_#{version} Test #{timestamp}") do |t|
          find('td', text: t)
        end

        parent = node.find(:xpath, '..')
        status_value = parent.all('td')[3].text
        expect(status_value).to eq 'In-service'
      end
    end
  end
end

