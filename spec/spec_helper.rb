Dir.glob("#{__dir__}/support/**/*.rb").each { |s| require s }
require 'colorize'
require 'capybara'
require 'capybara/rspec'
require 'pry'
require 'ruby-progressbar'

RSpec.configure do |config|
  config.include Capybara::DSL
  config.include FeatureHelper
  config.include Support
  $logger = Logger.new('rspec_log.log')
  $root_path = File.expand_path("../../", __FILE__)

  Capybara.run_server = false
  Capybara.app_host = "http://vps-staging.dropmysite.com"
  Capybara.default_max_wait_time = ENV['tout'].nil? ? 60 : ENV['tout'].to_i

  if ENV['FF']
    require 'selenium-webdriver'
    Capybara.default_driver = :selenium
    Capybara.javascript_driver = :selenium
  else
    require 'capybara/poltergeist'
    config.include Capybara::Poltergeist
    Capybara.default_driver = :poltergeist
    Capybara.javascript_driver = :poltergeist
    options = { js_errors: false, extensions: ["spec/support/bind.js"] }
    Capybara.register_driver :poltergeist do |app|
      Capybara::Poltergeist::Driver.new(app, options)
    end
  end


  config.expect_with :rspec do |expectations|
    # This option will default to `true` in RSpec 4. It makes the `description`
    # and `failure_message` of custom matchers include text for helper methods
    # defined using `chain`, e.g.:
    #     be_bigger_than(2).and_smaller_than(4).description
    #     # => "be bigger than 2 and smaller than 4"
    # ...rather than:
    #     # => "be bigger than 2"
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  # rspec-mocks config goes here. You can use an alternate test double
  # library (such as bogus or mocha) by changing the `mock_with` option here.
  config.mock_with :rspec do |mocks|
    # Prevents you from mocking or stubbing a method that does not exist on
    # a real object. This is generally recommended, and will default to
    # `true` in RSpec 4.
    mocks.verify_partial_doubles = true
  end
end
