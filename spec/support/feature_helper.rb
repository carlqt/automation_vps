module FeatureHelper
  def sign_in(email, password = '123456')
    visit '/en/sessions/new'
    fill_in 'Email', with: email
    fill_in 'Password', with: password
    click_button 'Sign In'
  end

  def create_server(&block)
    visit '/en/private_servers/new'
    settings = {}
    block.call(settings)

    server_name = "#{settings[:distribution]}_#{settings[:version]} Test #{settings[:timestamp]}"

    directory_field = all(".tagit-new")[0].find("input")
    exclusion_field = all(".tagit-new")[1].find("input")
    select settings[:distribution], from: 'private_server_distribution_id'
    select settings[:version], from: 'private_server_distribution_version_id'
    fill_in 'private_server_profile_name', with: server_name

    settings[:directories].split(',').map(&:strip).each { |dir| directory_field.set dir }
    settings[:exclusions].split(',').map(&:strip).each { |dir| exclusion_field.set dir }

    click_button 'Save'
    PrivateServer.create(settings)
  end

  def install_and_run_vps(file_url)
    case distro
    when 'Debian'
      `wget -O vps.deb \"#{file_url}\"`
      `sudo gdebi --n vps.deb`
      `sudo systemctl enable ServerBackup_client`
      `sudo systemctl start ServerBackup_client`
    when 'Ubuntu'
      `wget -O vps.deb \"#{file_url}\"`
      `sudo gdebi --n vps.deb`

      if version.to_f >= 15.04
        `sudo systemctl enable ServerBackup_client`
        `sudo systemctl start ServerBackup_client`
      else
        `sudo start ServerBackup_client`
      end

    when 'CentOS', 'Fedora'
      `wget -O vps.rpm \"#{file_url}\"`
      `sudo yum -y install vps.rpm`
      if version.to_f >= 7
        `sudo systemctl enable ServerBackup_client`
        `sudo systemctl start ServerBackup_client`
      else
        `sudo start ServerBackup_client`
      end
    when 'Fedora'
      `wget -O vps.rpm \"#{file_url}\"`
      `sudo yum -y install vps.rpm`
      `sudo systemctl enable ServerBackup_client`
      `sudo systemctl start ServerBackup_client`
    end
  end

  def verify_in_all_pages(text, &block)
    visit '/en/dashboard'

    if all("ul.pagination").empty?
      page_count = 0
    else
      page_count = find("ul.pagination").all("li").count - 3
      next_link = find("li.next a")
    end

    index = 0
    while index <= page_count
      sleep 2
      if page.text.match(text) || (page_count) == index
        return block.call(text)
      end
      index += 1
      next_link.click if all("li.next.disabled").empty?
    end
  end

  def create_test_files
    home = Dir.home
    FileUtils.mkdir_p "#{home}/test_files"
    FileUtils.mkdir_p "#{home}/test_files/exclude"
    5.times do |x|
      `dd if=/dev/urandom of=#{home}/test_files/sample#{x}.txt bs=10M count=1`
    end
  end

  def restored_contents(files)
    files.all? { |f| File.exist?("#{Dir.home}/test_files") }
  end

  def delete_files_from_test(files)
    files.each do |file|
      FileUtils.rm "#{Dir.home}/test_files/#{file}"
    end
  end

  def uninstall_client
    case distro
    when 'Ubuntu', 'Debian'
      `sudo apt-get -y purge ServerBackup`
    when 'CentOS', 'Fedora'
      `sudo rpm -e ServerBackup`
    end
  end

  def distro
    case
    when File.exist?('/etc/debian_version')
      `lsb_release -cd`.match("Ubuntu") ? "Ubuntu" : "Debian"
    when File.exist?('/etc/centos-release')
      'CentOS'
    when File.exist?('/etc/fedora-release')
      'Fedora'
    end
  end

  def version
    case distro
    when 'Ubuntu'
      dist = `lsb_release -cd`
      dist.scan(/\d+\.\d+/).first
    when 'Debian'
      dist = `lsb_release -cd`
      dist.scan(/\d+\.\d+/).first.to_i
    when 'CentOS'
      dist = `cat /etc/centos-release`
      dist.scan(/\d/).first
    when 'Fedora'
      dist = `cat /etc/fedora-release`
      dist.scan(/\d/).join
    end
  end

  def wait_for_progress
    timeout = Capybara.default_max_wait_time
    raise "Metadata is updating, please wait a few seconds" if current_url == "https://vps-staging.dropmysite.com/en/dashboard"
    puts current_url.yellow
    $logger.info "Initiating restore on #{current_url}"
    progress_bar = ProgressBar.create(format: "%t: %w")

    while progress_bar.progress < 100
      prev_progress = progress_bar.progress
      sleep timeout
      bar = find('.progress-bar')[:style]
      progress_bar.progress = bar.nil? ? 0 : bar.scan(/\d/).join.to_i

      if prev_progress == progress_bar.progress
        fatal_error! if page.text.match("Fatal error!")
        log_retry
        raise "#{current_url}: The progress is stuck or the timeout time is too short"
      end

      fatal_error! if page.text.match("Fatal error!")
    end
  end

  def select_dir(dir=nil)
    if dir
      dir.split('/').reject(&:empty?).each do |d|
        sleep 2
        find(".directory", text: "#{d}").click
      end
    end

    sleep 2
    check "parent_id"
  end

  def log_retry
    File.open(".retry", "w"){ |f| f.puts current_url }
  end

  def remove_retry_file
    if File.exist?('.retry')
      FileUtils.rm_rf('.retry')
    end
  end

  private

  def fatal_error!
    remove_retry_file
    raise "Restore encountered an error"
  end

  def files_equal?(old_files, new_files)
    old_files.map.with_index do |value, index|
      output = "comparing => #{value.name}"

      if value.eq?(new_files[index])
        # puts output.green
        true
      else
        puts output.yellow
        display_diverged_attributes(value)
        false
      end
    end.all?
  end

  def display_diverged_attributes(file)
    file.diff.each do |k, v|
      print "  #{k.yellow}".ljust(30)
      puts "#{v.first} -> #{v.last}".red
    end
  end

  def add_exc(dir=nil)
    new_dir = ""
    default = "/proc, /dev, /sys, /vagrant, /home/vagrant/vps_test"
    new_dir << "," if !dir.nil?
    new_dir << default
  end

  def add_dir(dir=nil)
    return "/" if dir.nil?
    dir
  end
end
