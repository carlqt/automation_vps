feature 'Observer vps-staging', js: true do
  background do
    visit '/'
    sign_in 'vps@dropmysite.com'
    sleep 20
  end

  let(:test_folder) { "#{Dir.home}/test_files" }
  let(:name) { PrivateServer.first.name }

  feature "When updating configuration" do
    it "sets status to 'Updating Configuration' or 'In-service'" do
      visit '/en/dashboard'
      node = verify_in_all_pages(name) do |t|
        find('td', text: t)
      end

      parent = node.find(:xpath, '..')
      edit_node = parent.all('td.chicklets a').first
      edit_node.click

      directory_field = all(".tagit-new")[0].find("input")
      directory_field.set "/home/vagrant/vps_test"
      click_button 'Save'

      node = verify_in_all_pages(name) do |t|
        find('td', text: t)
      end

      parent = node.find(:xpath, '..')
      status_value = parent.all('td')[3].text
      expect(status_value).to eq("In-service").or eq("Updating Configuration")

      if status_value == 'Updating Configuration'
        sleep 10
        visit '/en/dashboard'
        node = verify_in_all_pages(name) do |t|
          find('td', text: t)
        end

        parent = node.find(:xpath, '..')
        status_value = parent.all('td')[3].text
        expect(status_value).to eq("In-service")
      end
    end
  end

  feature "When restoring" do
    it 'status is \'Restoring\'' do
      visit '/en/dashboard'
      node = verify_in_all_pages(name) do |t|
        find('td', text: t)
      end

      parent = node.find(:xpath, '..')
      restore_node = parent.all('td.chicklets a')[2]
      restore_node.click

      expect(current_url).to match /restore=true/

      click_button 'restore-button'
      check 'agree'
      click_button 'Restore'

      node = verify_in_all_pages(name) do |t|
        find('td', text: t)
      end

      parent = node.find(:xpath, '..')
      status_value = parent.all('td')[3].text
      expect(status_value).to eq 'Restoring'
    end
  end

  feature "Excluded folder should not be displayed" do
    it "in webapp" do
      visit '/en/dashboard'
      node = verify_in_all_pages(name) do |t|
        find('td', text: t)
      end

      parent = node.find(:xpath, '..')
      restore_node = parent.all('td.chicklets a')[2]
      restore_node.click

      expect(current_url).to match /restore=true/

      find(".directory", text: "home").click
      find(".directory", text: "vagrant").click
      find(".directory", text: "test_files").click
      sleep 2

      excluded_directory = all(".directory", text: "exclude")
      expect(excluded_directory.empty?).to be true
    end
  end
end
