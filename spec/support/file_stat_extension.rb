module Support
  class FileStatExtension
    attr_reader :name, :uid, :gid, :rdev, :size, :blksize, :blocks, :mode, :diff

    def initialize(file)
      @file = File.lstat(file)
      @uid = @file.uid
      @gid = @file.gid
      @rdev = @file.rdev
      @size = @file.size
      @blksize = @file.blksize
      @blocks = @file.blocks
      @mode = @file.mode.to_s(8).to_i
      @name = file
      @diff = {}
    end

    def eq?(other)
      @diff = {}
      if other.is_a?(FileStatExtension)
        %w(uid gid rdev mode).each do |attr|
          save_diff_attribute(attr, other.send(attr)) if self.send(attr) != other.send(attr)
        end

        @diff.empty? ? true : false
      else
        self == other
      end
    end

    private

    def save_diff_attribute(attr, value)
      @diff[attr] = [self.send(attr), value]
    end
  end
end
