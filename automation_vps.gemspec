# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'automation_vps/version'

Gem::Specification.new do |spec|
  spec.name          = "automation_vps"
  spec.version       = AutomationVps::VERSION
  spec.authors       = ["Carl Tablante"]
  spec.email         = ["carlwilliam.tablante@gmail.com"]

  spec.summary       = %q{VPS wrapper for automation}
  spec.description   = %q{A series of vps features}
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.

  spec.files         = Dir["spec/**/*"] + %w(.rspec)
  spec.bindir        = "exe"
  spec.executables   = %w(vps)
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.10"
  spec.add_development_dependency "rake", "~> 10.0"
  spec.add_development_dependency "pry", "~> 0.10.3"
  spec.add_runtime_dependency "colorize", "~> 0.7.7"
  spec.add_runtime_dependency "rspec", "~> 3.4"
  spec.add_runtime_dependency "capybara", "~> 2.5"
  spec.add_runtime_dependency "poltergeist", "~> 1.8"
  spec.add_runtime_dependency "ruby-progressbar", "~> 1.7"

end
