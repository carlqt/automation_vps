feature 'vps-staging', js: true do
  background do
    visit '/'
  end

  let(:name) { PrivateServer.first.name }
  let(:dir) { ENV['dir'] }
  let(:retry_url) do
    if File.exist?(".retry") && !File.read(".retry").empty?
      File.read(".retry").strip
    end
  end


  feature "Download" do
    background do
      sign_in 'vps@dropmysite.com'
    end

    it 'downloads the directory and its contents' do
      if retry_url
        visit retry_url
      else
        visit '/en/dashboard'
        node = verify_in_all_pages(name) do |t|
          find('td', text: t)
        end

        parent = node.find(:xpath, '..')
        download_node = parent.find('td.chicklets').find("a[data-original-title='Download Files']")
        download_node.click

        expect(current_url).to match /private_servers/

        select_dir(dir)

        click_button 'download-button'
        fill_in 'private_server_download_name', with: name
        click_button 'Create'
      end

      wait_for_progress
      expect(page).to have_content 'Success! Your download is now ready'
    end
  end
end
