feature 'vps-staging', js: true do
  background do
    visit '/'
  end

  let(:timestamp) { Time.now.strftime('%Y-%m-%d_%H-%M-%S') }

  feature "Delete private server" do
    background do
      sign_in 'vps@dropmysite.com'
    end

    let(:name) { PrivateServer.first.name }

    it 'successfully deletes server' do
      visit '/en/dashboard'
      node = verify_in_all_pages(name) do |t|
        find('td', text: t)
      end

      parent = node.find(:xpath, '..')
      delete_node = parent.all('td.chicklets a').last
      server_number = delete_node[:href].gsub(/\D/, '')
      delete_node.click

      div = find("div#private_servers_delete_#{server_number}")
      form = div.find('form')
      form.find('input.btn-danger').click
      PrivateServer.first.delete
      uninstall_client

      expect(page.text).to have_content 'Your server was successfully deleted.'
    end
  end
end
