require 'csv'

class PrivateServer
  attr_accessor :name, :os, :timestamp, :version, :directories, :exclusions
  FILENAME = 'private_servers.csv'

  def self.create(settings={})
    server = self.new(distribution: settings[:distribution], version: settings[:version], timestamp: settings[:timestamp], directories: settings[:directories], exclusions: settings[:exclusions])
    server.save
  end

  def initialize(settings={})
    @version = settings[:version]
    @os = settings[:distribution]
    @timestamp = settings[:timestamp]
    @name = "#{os}_#{version} Test #{timestamp}"
    @exclusions = settings[:exclusions]
    @directories = settings[:directories]
  end

  def self.first
    server = CSV.read(PrivateServer::FILENAME).first
    self.new(version: server[1], distribution: server[0], timestamp: server[2], directories: server[3], exclusions: server[4])
  end

  def delete
    begin
      servers = CSV.read(PrivateServer::FILENAME)
      servers.shift
      csv = CSV.open(PrivateServer::FILENAME, 'wb')
      servers.each do |row|
        csv << row
      end
    rescue Exception => e
      puts e
    ensure
      csv.close
    end
  end

  def save
    server = CSV.open("#{$root_path}/private_servers.csv", 'wb') { |csv| csv << [@os, @version, @timestamp, @directories, @exclusions]}
    self
  end
end
