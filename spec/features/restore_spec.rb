feature 'vps-staging', js: true do
  background do
    visit '/'
  end

  let(:test_folder) { "#{Dir.home}/test_files" }
  let(:name) { PrivateServer.first.name }

  feature "Restore" do
    background do
      sign_in 'vps@dropmysite.com'
    end

    it 'finished properly in web app' do
      visit '/en/dashboard'
      node = verify_in_all_pages(name) do |t|
        find('td', text: t)
      end

      parent = node.find(:xpath, '..')
      restore_node = parent.all('td.chicklets a')[2]
      restore_node.click

      expect(current_url).to match /restore=true/

      find(".directory", text: "home").click
      find(".directory", text: "vagrant").click
      find(".directory", text: "test_files").click
      sleep 2

      filenames = page.text.scan(/sample\d\.txt/)

      expect(filenames.empty?).to be false
      delete_files_from_test(filenames)

      click_button 'restore-button'
      check 'agree'
      click_button 'Restore'

      expect(page).to have_content 'Restore succeed!'
      expect(restored_contents(filenames)).to eq true
    end
  end
end
