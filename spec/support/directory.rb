module Support
  class Directory
    attr_reader :root, :contents, :contents_stat

    def initialize(root='/')
      @root = "/#{root}".squeeze("/")
      @contents = contents
      @contents_stat = contents_stat
    end

    def contents
      return @contents unless @contents.nil?
      @contents = Dir.glob "#{root}/**/*".squeeze('/'), File::FNM_DOTMATCH
      @contents.sort.delete_if { |f| f.match(/(#{ignored_files})/) }
    end

    def contents_stat
      return @contents_stat unless @contents_stat.nil?
      contents.map{ |c| Support::FileStatExtension.new(c) }
    end

    private
    def ignored_files
      exclusions = PrivateServer.first.exclusions.split(", ")
      files = %w(history log .viminfo .netrwhist cache.txt /var) + exclusions
      files.join("|")
    end
  end
end

