feature 'vps-staging', js: true do
  background do
    visit '/'
  end

  let(:name) { PrivateServer.first.name }
  let(:dir) { ENV['dir'] }
  let(:retry_url) do
    if File.exist?(".retry") && !File.read(".retry").empty?
      File.read(".retry").strip
    end
  end

  feature "Restore" do
    background do
      sign_in 'vps@dropmysite.com'
    end

    it 'finished properly in web app' do
      if retry_url
        visit retry_url
      else
        visit '/en/dashboard'
        node = verify_in_all_pages(name) do |t|
          find('td', text: t)
        end

        before_files = Support::Directory.new(dir).contents_stat

        parent = node.find(:xpath, '..')
        restore_node = parent.find('td.chicklets').find("a[data-original-title='Restore Files']")
        restore_node.click

        expect(current_url).to match /restore=true/

        select_dir(dir)

        click_button 'restore-button'
        check 'agree'
        click_button 'Restore'
      end

      wait_for_progress
      expect(page).to have_content 'Restore succeed!'

      sleep 5
      after_files = Support::Directory.new(dir).contents_stat
      expect(files_equal?(before_files, after_files)).to be true
    end
  end
end
